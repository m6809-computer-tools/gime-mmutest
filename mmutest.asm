*
* Test program for a CoCo3/GIME compatible MMU
* Copyright 2016-2017 Tormod Volden
* Licensed under the GNU General Public License v3.0
*
* Build example:
* lwasm -b -o mmutest.bin -DCOCOBASIC mmutest.asm
*

* load at 6000-7FFF
	org	$6000
ourseg	fcb	0
befcrm	fcb	0
bank	fcb	0
bada	fcb	0
badb	fcb	0

entry
* ours from PC
	orcc #$50
	tfr pc,x
	lbsr adr2seg
	sta ourseg,pcr

* check if task registers are reset
	ldx #$FFA0
readtr	lda ,x+
	anda #$3F
	cmpa #$3F
	lbne failrst
	cmpx #$FFB0
	blo readtr

* reset MMU task registers
rstmmu	lda #0
	sta MMUCTL
	lda #$3F
	ldx #$FFA0
rsttr	sta ,x+
	cmpx #$FFB0
	blo rsttr
* read value from disabled CRM page (MMU disabled)
	lda $FE00
	nop		; will be read if no cart ROM present
	sta befcrm,pcr

* init overlay vectors (just copy BASIC vectors here, not CRM vectors)
	ldx #$FFF2
copyvec	lda #0
	sta MMUCTL
	ldb ,x
	lda #MMUEN+CRMEN
	sta MMUCTL
	stb ,x+
	cmpx #$FFFE
	blo copyvec
* write marker to page $3F
	ldb #99
	stb $FFF0
	decb
	stb $FFF1

* enable MMU (superfluous)
	lda #MMUEN+CRMEN
	sta MMUCTL

*write magic to CRM page
	lda #$55
	sta $FE01
*write bank number $FF to CRM
	lda #$FF
	sta $FE00
*verify read back while disabling CRM (MMU still enabled)
	lda #MMUEN
	sta MMUCTL
	lda $FE00
	nop
	cmpa befcrm,pcr
	* on MOOH, enabled MMU will have bank $3F here
	* lbne errcrmdis		* FIXME should check bank mem here?
	lda #MMUEN+CRMEN
	sta MMUCTL
	* FIXME test something here?
*verify read back while disabling MMU
	lda #CRMEN
	sta MMUCTL
	lda $FE00
	nop		; will be read if no cart ROM present
	cmpa befcrm,pcr
	lbne errmmudis
	lda #MMUEN+CRMEN
	sta MMUCTL
*verify read back
	lda $FE01
	cmpa #$55
	lbne errcrmena

* verify all banks and segments

* for all banks:
	ldb #BANKBEG
	* write into one segment
	* read check for every segment
newbank
	stb bank,pcr
	*map at E000
	stb $FFA7
	* write bank number and magic
	stb $E000
	lda #$AA
	sta $E001
	* for all segments (ours excluded)
	lda #SEGMBEG
newseg
	cmpa ourseg,pcr
	beq contseg
		* map at segment
	lbsr mapseg	; map bank b at segm a
		* read back and verify
	lbsr seg2adr
	cmpb ,x
	lbne errsegreadb
	ldb 1,x
	cmpb #$AA
	lbne errsegreadm
		* unmap at segment
	ldb #$3F
	bsr mapseg
	ldb bank,pcr
contseg
	inca
	cmpa #SEGMEND
	blo newseg
	* unmap at E000
	lda #$3F
	sta $FFA7

	* write check for every segment
	* for all segments (ours excluded)
		* map at segment
		* write bank number and magic2
		* map at E000
		* read back and verify
		* unmap at segment
		* unmap at E000

* verify common reserved memory
* for all banks:
	* map at E000
	stb $FFA7
	* verify previous bank number written to CRM
	decb
	cmpb $FE00
	lbrn errcrmback
	* write bank number to CRM
	incb
	stb $FE00
	* verify magic in CRM page
	ldb #$55
	cmpb $FE01
	lbrn errcrmmagic

	* default map at E000
	ldb #$3F
	stb $FFA7

	lda #'*'
	jsr OUTCHR
	ldb bank,pcr
	incb
	cmpb #BANKEND
	blo newbank

* finished OK
	lda #0
	sta MMUCTL
	andcc #~$50
	leax msgok,pcr
	leax -1,x
	jmp OUTSTR

BANKBEG equ	0
BANKEND	equ	$3F
SEGMBEG equ	0
SEGMEND	equ	8



MMUCTL	equ	$FF90
MMUEN	equ	$40
CRMEN	equ	$08

	IFNE 0
mmuena	pshs a
	lda MMUCTL
	ora #MMUEN
	sta MMUCTL
	puls a,pc

mmudis	pshs a
	lda MMUCTL
	anda #~MMUEN
	sta MMUCTL
	puls a,pc

crmena	pshs a
	lda MMUCTL
	ora #CRMEN
	sta MMUCTL
	puls a,pc

crmdis	pshs a
	lda MMUCTL
	anda #~CRMEN
	sta MMUCTL
	puls a,pc
	ENDC

* map bank B at seg A
mapseg	pshs x
	ldx #$FFA0
	stb a,x
	puls x,pc

seg2adr	pshs a,b
	lsla
	lsla
	lsla
	lsla
	lsla
	clrb
	tfr d,x
	puls a,b,pc

adr2seg	pshs b
	tfr x,d
	lsra
	lsra
	lsra
	lsra
	lsra
	puls b,pc

failrst
	leax msgmrst,pcr
	jsr OUTSTR
	lbra rstmmu

errmmudis
	leax msgdism,pcr
	bra fail
errcrmdis
	leax msgdis,pcr
	bra fail
errcrmena
	leax msgcrm,pcr
	bra fail
errcrmback
	leax msgcrmb,pcr
	bra fail
errcrmmagic
	leax msgcrmm,pcr
	bra fail
errsegreadb
errsegreadm
	leax msgmsg,pcr
fail
	sta bada,pcr
	stb badb,pcr
	lda #0
	sta MMUCTL
	andcc #~50
	leax -1,x
	jmp OUTSTR

msgmrst fcc /_NOT RESET/
	fcb $0d
	fcb 0
msgdism fcc /MMU /
msgdis	fcn /CRM DIS FAIL/
msgcrmm	fcc /MAGIC /
msgcrmb fcc /BACK /
msgcrm	fcn /CRM FAIL/
msgmsg	fcn /MMU FAIL/
msgok	fcb $0d
	fcn /MMU OK/

        IFDEF COCOBASIC
OUTSTR  equ $B99C
OUTCHR  equ $A282
        ELSE
OUTSTR	equ $90E5	; DRAGON
OUTCHR	equ $B54A	; DRAGON
        ENDC

	end entry
